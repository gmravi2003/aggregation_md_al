% makes plots

filepath='/home/gmravi/matlab_codes/aggregation_al_mirror_descent/';
active_data_file=strcat(filepath,'magic_scaled_avg_tst_err_num_points_seen_active.txt');
passive_data_file=strcat(filepath,'magic_scaled_tst_err_num_points_seen_passive.txt');
active_data=dlmread(active_data_file);
passive_data=dlmread(passive_data_file);

passive_data(1)=passive_data(2);

num_data=size(active_data,1);
xData=[1:num_data];
yData=[active_data';passive_data'];

options.ylimits=[0.25,0.70];
options.xlabel=['Number of points seen'];
options.ylabel=['Test Error Rate'];
options.legend={'MD-AL','Passive'};
options.colors={'r','b'};
options.legendLoc='NorthEast';


h=prettyPlot(xData,yData,options);
hold()
grid on;
set(gca, 'Position', get(gca, 'OuterPosition') - ...
            get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);
        
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [7.00 7.00]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 7.00 7.00]);

%saveas(h,strcat(figbasepath,'vc_upal_mnist'),'pdf');
%display(strcat('Figure saved at ',figbasepath,'all_mnist.pdf'));
