clear;
loss='mod-squared';
VALIDATION_FLAG=0;
parallelize=0;
[~, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    path='/home/gmravi/';
    NUMWORKERS=2;
else
    if(strcmp(strtrim(temp2),'no'))
        path='/net/hu17/gmravi/';
        NUMWORKERS=8;
    else
        % This is largo,
        path='/net/hu17/gmravi/';
        NUMWORKERS=6;
    end
end


FORCEFRESHPOOL=false;
if parallelize
        display('Going to create a pool....');
	matlabpool('close','force','local');
	pause(1);
	matlabpool('open','local',NUMWORKERS);
end
clear FORCEFRESHPOOL;
poolsize=matlabpool('size');
display(['The pool size is ',num2str(poolsize)]);


dirpath=strcat(path,'matlab_codes/iwal/wdbc/');
trn_data_file=strcat(dirpath,'wdbc_scaled_train_data.txt');

tst_data_file=strcat(dirpath,'wdbc_scaled_test_data.txt');
trn_labels_file=strcat(dirpath,'wdbc_train_labels.txt');
tst_labels_file=strcat(dirpath,'wdbc_test_labels.txt');

[full_trn_data,tst_data,full_trn_labels,tst_labels]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);


num_full_trn=size(full_trn_data,2);
num_tst=size(tst_data,2);

%full_trn_data=[full_trn_data;ones(1,num_full_trn)];
%tst_data=[tst_data;ones(1,num_tst)];

if(VALIDATION_FLAG==1)
    display('PERFORMING VALIDATION......');
    temp1=ceil(0.60*num_full_trn);
    trn_data=full_trn_data(:,1:temp1);
    trn_labels=full_trn_labels(1:temp1);
    
    valid_or_tst_data=full_trn_data(:,temp1+1:end);
    valid_or_tst_labels=full_trn_labels(temp1+1:end);
else
    display('PERFORMING TESTING.....');
    trn_data=full_trn_data;
    trn_labels=full_trn_labels;
    valid_or_tst_data=tst_data;
    valid_or_tst_labels=tst_labels;
end

stream = RandStream('mt19937ar','Seed',sum(100*clock));
RandStream.setDefaultStream(stream);
num_hypothesis_per_dim_vec=[80];
final_tst_err_vec=zeros(length(num_hypothesis_per_dim_vec),1);
opt_tst_err=2;
avg_tst_err_num_points_seen=zeros(size(trn_data,2),1);
avg_tst_loss_num_points_seen=zeros(size(trn_data,2),1);
for j=1:length(num_hypothesis_per_dim_vec)
    display('Started iterations...');
    num_hypothesis_per_dim=num_hypothesis_per_dim_vec(j);
    basis_models=DecisionStumpMatrix([trn_data,valid_or_tst_data],num_hypothesis_per_dim);
    [passive_struct]=...
        aggregation_passive_mirror_descent(trn_data,valid_or_tst_data,...
        trn_labels,valid_or_tst_labels,basis_models,loss);
    final_tst_err_vec(j)=passive_struct.tst_err_vec(end);
    display(size(passive_struct.tst_err_vec));
    display(size(avg_tst_err_num_points_seen));
    avg_tst_err_num_points_seen=avg_tst_err_num_points_seen+passive_struct.tst_err_vec;
    avg_tst_loss_num_points_seen=avg_tst_loss_num_points_seen+passive_struct.tst_loss_vec;
end

display('Num hypothesis are...');
display(num_hypothesis_per_dim_vec');
display('Test error is...');
display(final_tst_err_vec');
% Write to file
dlmwrite(['wdbc_scaled_tst_err_num_points_seen_passive_',loss,'.txt'],avg_tst_err_num_points_seen);

dlmwrite(['wdbc_scaled_tst_loss_num_points_seen_passive_',loss,'.txt'],avg_tst_loss_num_points_seen);

%display('Final test LOSS is ...');
%display(passive_struct.tst_loss_vec(end));


