% Performs convex aggregation of models, by actively generating a 
% labeled dataset 

% Provide as input the basis models. This should be a N*M matrix, where 
% N is the number of points in the stream, and M is the number of basis
% functions.

function [tst_err_vec,num_queries_made_vec,tst_loss_vec] = aggregation_al_mirror_descent(trn_data,tst_data,...
						     trn_labels,tst_labels,basis_models,mu,loss,mult_factor)

  al_struct=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,basis_models,mu,loss,mult_factor);

for iter=1:al_struct.num_trn
    
    %Update all the parameters
    %update beta_t, and epsilon_t values
    [al_struct]=UpdateBeta(al_struct,iter);
    [al_struct]=UpdateEpsilon(al_struct,iter);
    
    % Calculate Probability of querying
    prob=CalculateProbQuerying(al_struct,iter);
    
    % Random number generate and query
    rand_num=rand();
    Q_t=rand_num<prob;
    if(Q_t==1)
        %Query
        label=GetLabel(al_struct,iter);
        grad=CalculateGradient(prob,al_struct,label,iter);
      
        % We have to update the sum_grad field before performing
        % exponential updates
        [al_struct]=UpdateSumGrad(al_struct,grad);
    end
    [al_struct]=PerformExponentialUpdates(al_struct);
    % The next step, conceptually, should have been in update-structure
    % function. However update structure also updates the error rate. Hence
    % we are performing the below step outside the update-structure
    % function.
    
    tst_err=CalculateTestError(al_struct,iter); 
    tst_loss=CalculateTestLoss(al_struct,iter);
    al_struct=UpdateStructure(al_struct,iter,tst_err,tst_loss,prob,Q_t);
end
tst_err_vec=al_struct.tst_err_vec;
num_queries_made_vec=al_struct.num_queries_made_vec;
tst_loss_vec=al_struct.tst_loss_vec;
end


function[al_struct]=UpdateEpsilon(al_struct,iter)
al_struct.epsilon_t=iter^(-1*al_struct.mu);
end

function[al_struct]=UpdateBeta(al_struct,iter)

%beta_t= beta_0(t+1)^kappa
kappa=(1+al_struct.mu)/2;
al_struct.beta_t=al_struct.beta_0*((iter+1)/iter)^kappa;
end


function[label]=GetLabel(al_struct,iter)
label=al_struct.trn_labels(iter,1);
end


function[grad]=CalculateGradient(prob,al_struct,label,iter)
imp_weight=1/prob;
margin=label*al_struct.convex_agg'*al_struct.basis_models(iter:iter,:)';
grad=imp_weight*CalculateDerivative(al_struct,margin)*label*al_struct.basis_models(iter:iter,:)';
end

function[al_struct]=PerformExponentialUpdates(al_struct)

% The jth component is propotional to  exp(-g_j/beta)

comp=-al_struct.sum_grad/al_struct.beta_t;
comp_sub=comp-max(comp);
al_struct.convex_agg=exp(comp_sub)/sum(exp(comp_sub));

%if(abs(sum(al_struct.convex_agg)-1)>0.0001)
 %   display('Looks like there is a mistake in our update calculations.....');
 %   display('The resulting sum of convex aggregates looks as follows');
  %  display(sum(al_struct.convex_agg));
  %  CorrectMistake();
%end
end

function[al_struct]=UpdateSumGrad(al_struct,grad)
al_struct.sum_grad=al_struct.sum_grad+grad;
end


function[prob_plus]=CalculateProbPlus(al_struct,iter)
convex_agg=al_struct.convex_agg;
signed_sum=convex_agg'*al_struct.basis_models(iter:iter,:)';
%prob_plus=(1+signed_sum)/2;
prob_plus=EstimateCondProb(signed_sum,al_struct);
end



function[prob_plus]=EstimateCondProb(arg,al_struct)

loss=al_struct.LOSS;
if(strcmp(loss,'squared'))
   prob_plus=min(1,max(0,(1+arg)/2));
end

if(strcmp(loss,'mod-squared'))
    prob_plus=min(1,max(0,(1+arg)/2));
end
if(strcmp(loss,'exponential'))
    %1/1(1+exp(-2*arg))
    prob_plus=1/(1+exp(-2*arg));
end

if(strcmp(loss,'logistic'))
    prob_plus=1/(1+exp(-arg));
end

if(strcmp(loss,'mod-huber'))
    prob_plus=min(1,max(0,(1+arg)/2));
end
end

function[prob]=CalculateProbQuerying(al_struct,iter)
prob_plus=CalculateProbPlus(al_struct,iter);
prob=4*prob_plus*(1-prob_plus)*(1-al_struct.epsilon_t)+al_struct.epsilon_t;
end


function[al_struct]=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,basis_models,mu,LOSS,mult_factor)

al_struct=struct();
al_struct.trn_data=trn_data;
al_struct.tst_data=tst_data;
al_struct.trn_labels=trn_labels;
al_struct.tst_labels=tst_labels;
al_struct.mu=mu;
al_struct.LOSS=LOSS;

al_struct.num_trn=size(trn_data,2);
al_struct.num_tst=size(tst_data,2);
al_struct.num_dims=size(trn_data,1);
al_struct.basis_models=single(basis_models);
al_struct.num_basis=size(basis_models,2);


% Beta_0=(K^2L_{phi}^2/((2*mu+1)))*(T^{0.5+mu}/sqrt{T+1})*(1/log(M))
% We shall simply replace the constant K^2L_{phi}^2 with 1

L_phi=...
    max(abs(CalculateDerivative(al_struct,1)),...
            abs(CalculateDerivative(al_struct,-1)));

kappa=(1+al_struct.mu)/2;
denom=2^(kappa+1)*(al_struct.mu-kappa+1)*log(al_struct.num_basis);
beta_0_sqd=...
    L_phi^2/denom;
beta_0=sqrt(beta_0_sqd);

al_struct.beta_0=beta_0*mult_factor;
al_struct.beta_t=al_struct.beta_0;
al_struct.epsilon_t=1;

al_struct.convex_agg=ones(al_struct.num_basis,1)/al_struct.num_basis;
al_struct.sum_convex_agg=ones(al_struct.num_basis,1)/al_struct.num_basis;
al_struct.sum_grad=zeros(al_struct.num_basis,1);

% Some auxiliary quantities
al_struct.queried_indices=[];
al_struct.num_queried=0;
al_struct.imp_weights_queried=[];
al_struct.tst_err_vec=-1*ones(al_struct.num_trn,1);
al_struct.tst_loss_vec=-1*ones(al_struct.num_trn,1);
al_struct.num_queries_made_vec=ones(al_struct.num_trn,1);
end

function[deriv]=CalculateDerivative(al_struct,arg)
if(strcmp(al_struct.LOSS,'squared'))
    %LOSS=(1-p)^2
    % deriv=-2(1-p)
    deriv=-2*(1-arg);
end

if(strcmp(al_struct.LOSS,'mod-squared'))
    % deriv = -2*max(1-v)
    deriv=-2*max(0,1-arg);
end

if(strcmp(al_struct.LOSS,'exponential'))
    %LOSS=exp(-p)
    % deriv=-exp(-p)
    deriv=-exp(-arg);
end
if(strcmp(al_struct.LOSS,'hinge'))
    %LOSS=max{(1-p),0}
    %deriv=-1 if p<=1, 0 otherwise
    if(arg<=1)
        deriv=-1;
    else
        deriv=0;
    end
end
if(strcmp(al_struct.LOSS,'logistic'))
    %LOSS==log(1+exp(-p))
    %deriv=-exp(-p)/(1+exp(-p)) = -1/(1+exp(p))
    deriv=-1/(1+exp(arg));
end
if(strcmp(al_struct.LOSS,'mod-huber'))
    %deriv=max(min(2(v-1),0),-4)
    deriv=max(min(2*(arg-1),0),-4);
end
end


function[loss]=LossFunc(al_struct,arg)
if(strcmp(al_struct.LOSS,'squared'))
    %LOSS=(1-p)^2
    loss=(1-arg)^2;
end

if(strcmp(al_struct.LOSS,'mod-squared'))
    % deriv = -2*max(1-v)
    loss=(max(1-arg,0)).^2;
end

if(strcmp(al_struct.LOSS,'exponential'))
    loss=exp(-arg);
end
if(strcmp(al_struct.LOSS,'hinge'))
    %LOSS=max{(1-p),0}
    loss=max(1-arg,0);
end
if(strcmp(al_struct.LOSS,'logistic'))
    %LOSS==log(1+exp(-p))
    loss=log(1+exp(-arg));
end
if(strcmp(al_struct.LOSS,'mod-huber'))
    %deriv=max(min(2(v-1),0),-4)
    display('Do not have code for this...');
    exit();
   
end
end

function[al_struct]=UpdateStructure(al_struct,iter,tst_err,tst_loss,prob,Q_t)
al_struct.tst_err_vec(iter)=tst_err;
al_struct.tst_loss_vec(iter)=tst_loss;
if(Q_t==1)
    al_struct.queried_indices=[al_struct.queried_indices;iter];
    al_struct.imp_weights_queried=[al_struct.imp_weights_queried;1/prob];
    al_struct.num_queried=al_struct.num_queried+1;
end
al_struct.num_queries_made_vec(iter,1)=al_struct.num_queried;
al_struct.sum_convex_agg=al_struct.sum_convex_agg+al_struct.convex_agg;
end

function[tst_err]=CalculateTestError(al_struct,iter)
avg_convex_agg=al_struct.sum_convex_agg/iter;
pred=sign(al_struct.basis_models(al_struct.num_trn+1:end,:)*avg_convex_agg);

tst_err=length(find(pred.*al_struct.tst_labels==-1));
tst_err=tst_err/al_struct.num_tst;
end

function[tst_loss]=CalculateTestLoss(al_struct,iter)
avg_convex_agg=al_struct.sum_convex_agg/iter;
real_pred=al_struct.basis_models(al_struct.num_trn+1:end,:)*avg_convex_agg;

tst_loss=sum(LossFunc(al_struct,real_pred.*al_struct.tst_labels));
tst_loss=tst_loss/al_struct.num_tst;
end
