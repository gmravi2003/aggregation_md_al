function[basis_models,models_array] =DecisionStumpMatrix(data,num_hyp_per_dim)
num_dims=size(data,1);

% We can use only the range of the data to create models

min_max_mat=[min(data,[],2),max(data,[],2)];
num_basis_models=2*num_hyp_per_dim*num_dims;
[models_array]=CreateArrayOfStructures(data,num_basis_models,...
                                            num_hyp_per_dim,min_max_mat);

[basis_models]=int8(Predict(models_array,data));
end



function[basis_models]=Predict(models_array,data)

num_models=size(models_array,2);
%display(num_models);
basis_models=zeros(size(data,2),num_models);
for j=1:num_models
    dim_to_use=models_array(j).dim;
    threshold=models_array(j).threshold;
    model_prediction=models_array(j).prediction;
    basis_models(:,j:j)=(model_prediction*(2*(data(dim_to_use:dim_to_use,:)<threshold)-1))';
end
end

function[models_array]=CreateArrayOfStructures(data,num_basis_models,...
                                                num_hyp_per_dim,min_max_mat)
num_dims=size(data,1);

model_num=1;
models_array(num_basis_models).dim=0;
for d=1:num_dims
    min_val=min_max_mat(d,1);
    max_val=min_max_mat(d,2);
    diff=(max_val-min_val)/num_hyp_per_dim;
    thresholds=[min_val:diff:max_val];
    for i=1:length(thresholds)
        models_array(model_num).dim=d;
        models_array(model_num).threshold=thresholds(i);
        models_array(model_num).prediction=1;
        model_num=model_num+1;
        % Also add the negation of the above hypothesis
        
        models_array(model_num).dim=d;
        models_array(model_num).threshold=thresholds(i);
        models_array(model_num).prediction=-1;
        model_num=model_num+1;
    end
end
%There might be some empty elements, as in some dimensions values may not always be unique.
%Remove them
models_array(model_num:end)=[];
end
