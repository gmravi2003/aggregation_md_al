clear;
VALIDATION_FLAG=0;
loss='mod-squared';
parallelize=1;
[~, temp2]=system('hostname');
if(strcmp(strtrim(temp2),'leibniz'))
    path='/home/gmravi/';
    NUMWORKERS=2;
else
    if(strcmp(strtrim(temp2),'no'))
        path='/net/hu17/gmravi/';
        NUMWORKERS=4;
    else
        % This is largo,
        path='/net/hu17/gmravi/';
        NUMWORKERS=6;
    end
end

FORCEFRESHPOOL=false;
if parallelize
        display('Going to create a pool....');
	matlabpool('open','local',NUMWORKERS);
end
clear FORCEFRESHPOOL;
poolsize=matlabpool('size');
display(['The pool size is ',num2str(poolsize)]);

dirpath=strcat(path,'matlab_codes/iwal/wdbc/');
trn_data_file=strcat(dirpath,'wdbc_scaled_train_data.txt');
tst_data_file=strcat(dirpath,'wdbc_scaled_test_data.txt');
trn_labels_file=strcat(dirpath,'wdbc_train_labels.txt');
tst_labels_file=strcat(dirpath,'wdbc_test_labels.txt');

display(tst_labels_file);

[full_trn_data,tst_data,full_trn_labels,tst_labels]=...
    ReadData(trn_data_file,tst_data_file,trn_labels_file,tst_labels_file);

num_full_trn=size(full_trn_data,2);
num_tst=size(tst_data,2);

full_trn_data=[full_trn_data;ones(1,num_full_trn)];
tst_data=[tst_data;ones(1,num_tst)];

if(VALIDATION_FLAG==1)
    display('PERFORMING VALIDATION......');
    temp1=ceil(0.60*num_full_trn);
    trn_data=full_trn_data(:,1:temp1);
    trn_labels=full_trn_labels(1:temp1);

    valid_or_tst_data=full_trn_data(:,temp1+1:end);
    valid_or_tst_labels=full_trn_labels(temp1+1:end);
else
    display('PERFORMING TESTING.....');
    trn_data=full_trn_data;
    trn_labels=full_trn_labels;
    valid_or_tst_data=tst_data;
    valid_or_tst_labels=tst_labels;
end


stream = RandStream('mt19937ar','Seed',sum(100*clock));
RandStream.setDefaultStream(stream);
NUM_REPEATS=10;
mu=0.3;
num_hypothesis_per_dim=80;
mult_factor=0.5;
display(mult_factor);
avg_num_queries_vec=zeros(size(trn_data,2),1);  
display(mu);

avg_tst_err_num_points_seen=zeros(size(trn_data,2),1);
avg_tst_loss_num_points_seen=zeros(size(trn_data,2),1);

basis_models=DecisionStumpMatrix([trn_data,valid_or_tst_data],num_hypothesis_per_dim);
display('Formed models');
display(size(basis_models));

parfor repeat=1:NUM_REPEATS
    display('Started iteration....');
    [tst_err_vec,num_queries_made_vec,tst_loss_vec]=aggregation_al_mirror_descent(trn_data,valid_or_tst_data,...
					      trn_labels,valid_or_tst_labels,basis_models,mu,loss,mult_factor);
    avg_tst_err_num_points_seen=avg_tst_err_num_points_seen+tst_err_vec;
    avg_num_queries_vec=avg_num_queries_vec+num_queries_made_vec;
    avg_tst_loss_num_points_seen=...
        avg_tst_loss_num_points_seen+tst_loss_vec;
end

display(mult_factor);
avg_tst_err_num_points_seen=avg_tst_err_num_points_seen/ ...
    NUM_REPEATS;

avg_tst_loss_num_points_seen=avg_tst_loss_num_points_seen/ ...
   NUM_REPEATS;

avg_num_queries_vec=avg_num_queries_vec/NUM_REPEATS;
display('Average number of queries made are ...');
display(avg_num_queries_vec(end));

display('Test error at the end is ...');
display(avg_tst_err_num_points_seen(end));

display('Test loss at the end is....');
display(avg_tst_loss_num_points_seen(end))
display(mult_factor);
dlmwrite(['wdbc_scaled_avg_num_queries_',loss,'.txt'],avg_num_queries_vec);
dlmwrite(['wdbc_scaled_avg_tst_err_num_points_seen_active_',loss,'.txt'],avg_tst_err_num_points_seen);
dlmwrite(['wdbc_scaled_avg_tst_loss_num_points_seen_active_',loss,'.txt'],avg_tst_loss_num_points_seen);

matlabpool('close');
%{
avg_final_tst_err_mat=cell2mat(avg_final_tst_err_cell);
[min_err,ind]=min(avg_final_tst_err_mat(:));

[indx_opt_num_hypothesis_per_dim,indx_opt_mu]=...
    ind2sub(size(avg_final_tst_err_mat),ind);
opt_num_hypothesis_per_dim=num_hypothesis_per_dim_vec(indx_opt_num_hypothesis_per_dim);
opt_mu=mu_vec(indx_opt_mu);
display('Optimal settings are....');
display(opt_num_hypothesis_per_dim);
display(opt_mu);
display(min_err);
display(tst_labels_file);
%}
