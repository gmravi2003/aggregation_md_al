% Performs convex aggregation of models, by actively generating a 
% labeled dataset 

% Provide as input the basis models. This should be a N*M matrix, where 
% N is the number of points in the stream, and M is the number of basis
% functions.

function [pass_struct] = aggregation_passive_mirror_descent(trn_data,tst_data,...
    trn_labels,tst_labels,basis_models,loss)

LOSS=loss;
pass_struct=CreateStructure(trn_data,tst_data,trn_labels,...
                                tst_labels,basis_models,LOSS);

for iter=1:pass_struct.num_trn
    
    %Update all the parameters
    %update beta_t, value
    [pass_struct]=UpdateBeta(pass_struct,iter);
    label=pass_struct.trn_labels(iter,1);
    grad=CalculateGradient(pass_struct,label,iter);
    
    % We have to update the sum_grad field before performing
    % exponential updates
    
    [pass_struct]=UpdateSumGrad(pass_struct,grad);
    [pass_struct]=PerformExponentialUpdates(pass_struct);
    
    % The next step, conceptually, should have been in update-structure
    % function. However update structure also updates the error rate. Hence
    % we are performing the below step outside the update-structure
    % function.
    tst_err=CalculateTestError(pass_struct,iter);
    tst_loss=CalculateTestLoss(pass_struct,iter);
    
    pass_struct.sum_convex_agg=...
        pass_struct.sum_convex_agg+pass_struct.convex_agg;

    pass_struct.tst_err_vec(iter)=tst_err;
    pass_struct.tst_loss_vec(iter)=tst_loss;
end
end




function[pass_struct]=UpdateBeta(pass_struct,iter)

%beta_t= beta_0\sqrt{t+1}

pass_struct.beta_t=pass_struct.beta_0*sqrt(iter+1);
end

function[grad]=CalculateGradient(pass_struct,label,iter)
margin=label*pass_struct.convex_agg'*pass_struct.basis_models(iter:iter,:)';
grad=CalculateDerivative(pass_struct,margin)*label*pass_struct.basis_models(iter:iter,:)';
end

function[pass_struct]=PerformExponentialUpdates(pass_struct)

% The jth component is propotional to  exp(-g_j/beta)

comp=-pass_struct.sum_grad/pass_struct.beta_t;
comp_sub=comp-max(comp);
pass_struct.convex_agg=exp(comp_sub)/sum(exp(comp_sub));

%if(abs(sum(pass_struct.convex_agg)-1)>0.00001)
 %   display('Looks like there is a mistake in our update calculations.....');
  %  display('The resulting updates looks as follows');
  %  display(pass_struct.convex_agg);
  %  CorrectMistake();
%end
end

function[pass_struct]=UpdateSumGrad(pass_struct,grad)
pass_struct.sum_grad=pass_struct.sum_grad+grad;
end



function[pass_struct]=CreateStructure(trn_data,tst_data,trn_labels,tst_labels,basis_models,LOSS)

pass_struct=struct();
pass_struct.trn_data=trn_data;
pass_struct.tst_data=tst_data;
pass_struct.trn_labels=trn_labels;
pass_struct.tst_labels=tst_labels;
pass_struct.LOSS=LOSS;

pass_struct.num_trn=size(trn_data,2);
pass_struct.num_tst=size(tst_data,2);
pass_struct.num_dims=size(trn_data,1);
pass_struct.basis_models=single(basis_models);
pass_struct.num_basis=size(basis_models,2);


% Beta_0=(K^2L_{phi}^2)*(1/log(M))
% We shall simply replace the constant K^2L_{phi}^2 with 1

lip_constant=...
    max(abs(CalculateDerivative(pass_struct,-1)),...
       abs(CalculateDerivative(pass_struct,1)));

%lip_constant=0.1;
display(lip_constant);
beta_0_sqd=(lip_constant^2/(log(pass_struct.num_basis)));
beta_0=sqrt(beta_0_sqd);

pass_struct.beta_0=beta_0;
pass_struct.beta_t=beta_0;

pass_struct.convex_agg=ones(pass_struct.num_basis,1)/pass_struct.num_basis;
pass_struct.sum_convex_agg=ones(pass_struct.num_basis,1)/pass_struct.num_basis;
pass_struct.sum_grad=zeros(pass_struct.num_basis,1);

% Some auxiliary quantities
pass_struct.tst_err_vec=-1*ones(pass_struct.num_trn,1);
pass_struct.tst_loss_vec=-1*ones(pass_struct.num_trn,1);
end

function[deriv]=CalculateDerivative(pass_struct,arg)

if(strcmp(pass_struct.LOSS,'squared'))
    %LOSS=(1-p)^2
    % deriv=-2(1-p)
    deriv=-2*(1-arg);
end

if(strcmp(pass_struct.LOSS,'mod-squared'))
    %LOSS=(1-p)^2
    % deriv=-2(1-p)
    deriv=-2*max(0,1-arg);
end


if(strcmp(pass_struct.LOSS,'exponential'))
    %LOSS=exp(-p)
    % deriv=-exp(-p)
    deriv=-exp(-arg);
end
if(strcmp(pass_struct.LOSS,'hinge'))
    %LOSS=max{(1-p),0}
    %deriv=-1 if p<=1, 0 otherwise
    if(arg<=1)
        deriv=-1;
    else
        deriv=0;
    end
end
if(strcmp(pass_struct.LOSS,'logistic'))
    %LOSS==log(1+exp(-p))
    %deriv=-exp(-p)/(1+exp(-p)) = -1/(1+exp(p))
    deriv=-1/(1+exp(arg));
end
end


function[loss]=LossFunc(pass_struct,arg)
if(strcmp(pass_struct.LOSS,'squared'))
    %LOSS=(1-p)^2
    loss=(1-arg)^2;
end

if(strcmp(pass_struct.LOSS,'mod-squared'))
    % deriv = -2*max(1-v)
    loss=(max(1-arg,0)).^2;
end

if(strcmp(pass_struct.LOSS,'exponential'))
    loss=exp(-arg);
end
if(strcmp(pass_struct.LOSS,'hinge'))
    %LOSS=max{(1-p),0}
    loss=max(1-arg,0);
end
if(strcmp(pass_struct.LOSS,'logistic'))
    %LOSS==log(1+exp(-p))
    loss=log(1+exp(-arg));
end
if(strcmp(pass_struct.LOSS,'mod-huber'))
    %deriv=max(min(2(v-1),0),-4)
    display('Do not have code for this...');
    exit();
   
end
end


function[tst_err]=CalculateTestError(pass_struct,iter)
base_models_pred=pass_struct.basis_models(pass_struct.num_trn+1:end,:);
avg_convex_agg=pass_struct.sum_convex_agg/iter;
pred=sign(base_models_pred*avg_convex_agg);

tst_err=length(find(pred.*pass_struct.tst_labels==-1));
tst_err=tst_err/pass_struct.num_tst;
end

function[tst_loss]=CalculateTestLoss(pass_struct,iter);
base_models_pred=pass_struct.basis_models(pass_struct.num_trn+1:end,:);
avg_convex_agg=pass_struct.sum_convex_agg/iter;
real_pred=base_models_pred*avg_convex_agg;

tst_loss=sum(LossFunc(pass_struct,real_pred.*pass_struct.tst_labels));
tst_loss=tst_loss/pass_struct.num_tst;
end

